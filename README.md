# LabelImge Extended Tool

Extended Open-Source Label Image Annotation Tool called Labelimge "labelImg.py" with variant cross-platforms required at least Python 3.6 and has been tested with PyQt 4.8, additionally achieves Detection & Multi-Tracking Objects.

## Dependencies
requires at least `Python 3+` and has been tested with `PyQt 4.8`. However, `opencv-contrib-python` and `lxml` are strongly recommended to run the extended LabelImg on Amphora Detection & MultiTracking Approches.
###### Windows
- [ ] install [python](https://www.python.org/downloads)
- [ ] install [ PyQt5](https://www.riverbankcomputing.com/software/pyqt/download)
- [ ] install [lxml](https://lxml.de/installation.html)
- [ ] install [opencv-contrib-python](https://pypi.org/project/opencv-contrib-python/)

 if items were added in files in the resources/strings folder:

     ```
      conda install pyqt=5
      conda install -c anaconda lxml
      pyrcc5 -o libs/resources.py resources.qrc
      python labelImg.py
      python labelImg.py [IMAGE_PATH] [PRE-DEFINED CLASS FILE]
     ```

## Getting the .CFG File, .Weights File, Obj.NAMES File

- [ ] [obtaining .cfg](https://github.com/AlexeyAB/darknet#datasets)
- [ ] [obtaining .weights](https://github.com/AlexeyAB/darknet#datasets)
- [ ] [obtaining .names](https://github.com/AlexeyAB/darknet#datasets)

Store the files in place where you will be able to uploaded to the Labelimage Extended Tool later on.

## Detection Phase 
> YOLO (You Only Look Once) is an object detection algorithm that allows to detect objects in an images in near real-time. YOLOv4 is 4th version of YOLO which introduced in April 2020.
- [YoloV4] - [YoloV4](https://github.com/AlexeyAB)
## Tracking phase 
> OpenCV’s multi-object tracking API implemented using the MultiTracker class  in Python, in order to implement its featurized parametric obligedly needs OpenCV Library with latest Version.
- [MultiTracking] - [MultiTracking](https://github.com/adipandas/multi-object-tracker)
subsidaries are as describe herebelow.

| Function name | Description                    |
| ------------- | ------------------------------ |
| `o` or  `O`   | Upload Prerequisites Files     |
| `y` or `Y`    | apply the Detection Mode       |
| `n` or `N`    | apply the Tracking Mode        |
| `p` or `P`    | help                           |
| ctrl+shift+T  | Activate the single tracking mode|

## Contact Info
<maad.alanni@gitlab.lis-lab.fr>

